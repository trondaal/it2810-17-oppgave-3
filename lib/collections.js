/*
initializes the collections used in this application
*/
import {Mongo} from 'meteor/mongo';

export const Items = new Mongo.Collection('items');
export const Categories = new Mongo.Collection('categories');
export const stuff = new Mongo.Collection('stuff');
