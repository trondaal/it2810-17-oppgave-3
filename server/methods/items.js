/*
Methods defining the attributes of the collection
*/
import {Categories, Items} from '/lib/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

export default function () {
  Meteor.methods({
    'categories.create'(name){
      check(name, String);
      const createdAt = new Date();
      const category = {name, createdAt};
      Categories.insert(category);
    }
  });
/*This method checks if the input parameters is correct and throws error if not.
 If everything is ok it inserts the data to the database*/
  Meteor.methods({
    'items.create' (name, rating, priceLevel, type, vicinity) {
      check(name, String);
      check(type, String);
      check(rating, Number);
      check(priceLevel, Number);
      check(vicinity, String)
      const id = Meteor.userId();
      const createdAt = new Date();
      const restaurant = {name, rating, priceLevel, type, vicinity, createdAt, id};
      if(!Meteor.userId()){
        throw new Meteor.Error('403', 'Forbidden');
      }else{
        Items.insert(restaurant);
      }
    }
  });
//Removes restaurant from collection.
  Meteor.methods({
    'items.remove'(id){
      check(id, String);
      Items.remove(id);
    }
  });

}
