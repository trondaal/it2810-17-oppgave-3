/*
the main file on the server side initializes the collections
*/
import publications from './publications';
import methods from './methods';

publications();
methods();
