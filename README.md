# README #

Resturapp is an application for viewing/seraching local restaurants in your area. We offer a map view implemented view google maps api, and a user login where users can store their favorite places. 

Link: http://it2810-17.idi.ntnu.no/

### What is this repository for? ###
IT2810

* Quick summary:
front-end developement in this application is done with [react boostrap](https://react-bootstrap.github.io)
* Version:
Divided between multiple repositories on bitbucket and github, this is version 4 of our meteor application

### Architecture ###

The picture show the application architecture.

![Architecture.png](https://bitbucket.org/repo/9rKb4z/images/6743030-Architecture.png)


### How do I get set up? ###
Install [meteor](https://www.meteor.com) on your machine
Clone the repo 
run: 
```
#!shell

npm install 
```
```
#!shell

meteor
```


* Summary of set up
The architecture of this application is based on [mantra](https://github.com/kadirahq/mantra)
* Configuration
* Dependencies
Every component, container, module etc. that is used in this project is imported/exported through the index.js files that are found in multiple locations throught the application structure. 
You will find all the dependecies in package.json and .meteor/packages. All depencies in this project are added with: 

```
#!shell

meteor npm install --save <package name> 
```

* Database configuration
To start the database in CLI-mode run the following: 

```
#!shell
meteor mongo
// to reset the database run: 
meteor reset
```
We have also used an extesion called [mongol](https://github.com/msavin/Mongol). This has helped us a great deal to view the continous change of the database while we are using the application. 

* How to run tests
* Deployment instructions

On our deployment server we run a nginx web server wich acts as a reverse proxy towards localhost:3000 so that meteor actually runs on port 80 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

### Known bugs :( ###
* Should've added if-statements so the "add to favorites" buttons is unavailable if the user isn't logged in.
* Enter key to submit search after a place doesn't work, you have to click the autocompleted text
* In "My Favorites": Sorting and filtering doens't work together with dynamic loading. To get dynamic loading to work. Comment out line 167 and uncomment line 168 and 172-177 in client/modules/items/components/ItemList.jsx
* You can add multiple of the same restaurants to favorites
* Documentation link in the navbar doesn't work. It was going to link to this page.
* Buggy login with Facebook and Twitter. Didn't remove in time for due time.