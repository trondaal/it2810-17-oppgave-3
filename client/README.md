## Documentation client

The front-end in this project is developed with ReactJS. Bootstrap is used as framework for the design, together with CSS. The folder Client contains all the files. Client is divided into three folders configs, modules and stylesheets. The client folder also contains the main.js file that is the main file for the client side and initializes the application. The stylesheet folder consists of style.css, which is used to style the whole application.  

The modules folder contains all the main files for the frontend part, and is divided into core, items and users. The core folder has the “main” files, MainLayout.jsx which loads all the content on the page. Footer.jsx is the footer and NavBar.jsx is the navbar used to navigate on the website. Routes.jsx is used for routing. When the user clicks on a link in the navbar this is the file that routes the user to the correct page. The item folder is divided into actions and components. The action was used so that the containers could run action (database handling) and at the same time show the components.

The component folder contains all the components. All the .jsx files is used as components in the application. Some examples is MapView.jsx which displays the map and the SearchBar.jsx which is the searchbar used to search for places. The containers folder contains the frontend logic.

The last folder in client is configs. This folder contains the file context.jsx  which return an object needed for the application. In this file all the collections is included and the core ES2015 modules (Meteor, FlowRouter, ReactiveDict and Tracker). All this is then initialized in main.js.
