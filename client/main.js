/*
main file on the client side initializes the application
*/
//session
import { Tracker } from 'meteor/tracker';
import { Session } from 'meteor/session';
let sessionId = 0;

console.log(Tracker + Session + ' her er trakerene og sessions');
console.log(Session.get());

import {createApp} from 'mantra-core';
import initContext from './configs/context';

// modules
import coreModule from './modules/core';
import itemsModule from './modules/items';

// init context
const context = initContext();

Meteor.startup(() => {
  // code to run on server at startup
  console.log('server running');
});

// create app
const app = createApp(context);
app.loadModule(coreModule);
app.loadModule(itemsModule);
app.init();

// given the user a session upon log in
Tracker.autorun(()=> {
  if (Meteor.loggingIn()){
    if(Meteor.user() != null){
      console.log("logging in");
      Session.set('stuff' + sessionId);
      console.log('Session sett');
    }
  }
    sessionId++;
});
