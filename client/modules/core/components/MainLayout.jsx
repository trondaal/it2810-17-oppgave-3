import React from 'react';
import { Grid, Row, Col, PageHeader} from 'react-bootstrap';
import ItemProgress from '../../items/components/ItemProgress.jsx';
import Navbar from './NavBar.jsx';
import Footer from './Footer.jsx';

const Layout = ({content}) => (
  <div className = 'body'>
  <Navbar />
  <Grid className ='main'>
    {content()}
  </Grid>
  <Footer />
  </div>
);

export default Layout;
