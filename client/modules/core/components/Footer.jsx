import React from 'react';
import {Modal, Navbar,Nav, NavItem, Panel} from 'react-bootstrap';


const Footer = ({content}) => (
  <footer className="page-footer">
         <div className="container">
           <div className="row">
             <div className="col l6 s12">
               <h5 className="white-text"></h5>
               <p className="grey-text text-lighten-4">
                 The source code for this application is avaliable in our bitbucket repository
               </p>
             </div>
             <div className="col l4 offset-l2 s12">
               <h5 className="white-text">Developers</h5>
               <p> Hung, Terje, Øystein, Kathrine og Henry
               </p>
             </div>
           </div>
         </div>
         <div className="footer-copyright">
           <div className="container">
           © 2016 Group 17 TDT2810
           <a className="grey-text text-lighten-4 right" href="https://bitbucket.org/trondaal/it2810-17-oppgave-3"> Our code</a>
           </div>
         </div>
       </footer>

);

export default Footer;
