import React from 'react';
import {Navbar, Nav, NavItem} from 'react-bootstrap';

const LoginButton = BlazeToReact('loginButtons');

const NavBar = ({content}) => (
  <Navbar className = 'navbar' inverse collapseOnSelect>
    <Navbar.Header>
      <Navbar.Brand>
        <a href="/"> Restaurapp </a>
      </Navbar.Brand>
      <Navbar.Toggle className = 'toggle'/>
    </Navbar.Header>
    <Navbar.Collapse className= 'collapse'>
    <Nav className = 'navbarItems'>
      <NavItem href="/myfavorites"> My Favorites</NavItem>
      <NavItem> <LoginButton /> </NavItem>
      <NavItem> Documentation </NavItem>
    </Nav>
    </Navbar.Collapse>
  </Navbar>
);

export default NavBar;
