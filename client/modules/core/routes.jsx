/*
routes does the routing of the entire application, every componenent is imported here and
given a path that is referenced in the navBar component
*/

import React from 'react';
import {mount} from 'react-mounter';

import Layout from './components/MainLayout.jsx';
import ItemList from '../items/containers/ItemList.js';
import EditItem from '../items/containers/EditItem.js';
import CategoryList from '../items/containers/CategoryList.js';
import NewCategory from '../items/containers/NewCategory.js';

import MapView from '../items/components/MapView.jsx';

export default function (injectDeps, {FlowRouter}) {
  const MainLayoutCtx = injectDeps(Layout);

  //Router for homepage "/"
  FlowRouter.route('/', {
    name: 'items.list',
    action() {
      mount(MainLayoutCtx, {
        content: () => (<MapView />)
      });
    }
  });

  FlowRouter.route('/myfavorites', {
    name: 'items.list',
    action() {
      mount(MainLayoutCtx, {
        content: () => (<ItemList />)
      });
    }
  });

  FlowRouter.route('/edit/:itemId', {
    name: 'items.edit',
    action({itemId}){
      mount(MainLayoutCtx, {
        content: () => (<EditItem itemId={itemId} />)
      });
    }
  })

  //Router for editpage "/edit"
  FlowRouter.route('/edit', {
    name: 'items.edit',
    action() {
      mount(MainLayoutCtx, {
        content: () => (<EditItem />)
      });
    }
  });

  FlowRouter.route('/categories', {
    name: 'categories.list',
    action() {
      mount(MainLayoutCtx, {
        content: () => (<CategoryList />)
      });
    }
  });

  FlowRouter.route('/categories/new/', {
    name: 'categories.new',
    action() {
      mount(MainLayoutCtx, {
        content: () => (<NewCategory />)
      });
    }
  });
}
