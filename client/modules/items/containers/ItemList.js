import ItemList from '../components/ItemList.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

//Composer fetcher data
export const composer = ({context}, onData) => {
  const {Meteor, Collections} = context();
  if (Meteor.subscribe('items.list').ready()) {
    /*({id: Meteor.userId()}, {sort: {createdAt: -1}}) is sorting the displayal of items after which one was created first for
    the logged in user. For rating it displays after highest rated first.*/
    const dateAdded = Collections.Items.find({id: Meteor.userId()}, {sort: {createdAt: -1}}).fetch();
    const rating = Collections.Items.find({id: Meteor.userId()}, {sort: {rating: 1}, limit: 9}).fetch();

    /*({id: Meteor.userId(), type: "bar"}}, {sort: {createdAt: -1}}) is filtering the displayal of items after which one was created first for
    the logged in user and filtering on items with the type "bar" or restaurant.*/
    const filterBar = Collections.Items.find({id: Meteor.userId(), type: "bar"}, {sort: {createdAt: -1}}).fetch();
    const totalItems = Collections.Items.find().count();
    const filterRating = Collections.Items.find({id: Meteor.userId(), rating: {$gt: 3.9}}, {sort: {createdAt: -1}}).fetch();

    function nextItems(loadedItems){
      return Collections.Items.find({}, { skip: loadedItems, limit: 9});
    }
    function removeItem(id){
      return Collections.Items.remove({_id: id});
    }
    onData(null, {dateAdded, rating, filterBar, filterRating, totalItems, nextItems, removeItem});

  }
};

export default composeAll(
  composeWithTracker(composer),
  useDeps()
)(ItemList);
