import React, {PropTypes as T} from 'react';
import Map, {Marker} from 'google-maps-react';

export class MapComponent extends React.Component {
  renderMarkers(){
    if(!this.props.places) {return null;}
    return this.props.places.map(place => {
      return <Marker key={place.id} name={place.id} place={place} onClick={this.props.onMarkerClick} position={place.geometry.location}/>
    })
  }

  render(){
    const {places, center, selectedPlace, selectedVicinity} = this.props
    return (
      <div className='mapComponent'>
        <p>Selected place: <b>{this.props.selectedPlace}</b> {this.props.selectedVicinity}</p>
        <Map google={this.props.google} center={this.props.center} containerStyle={{ width: '100%', height: '75%'}}>
          {this.renderMarkers()}
        </Map>

      </div>
    )
  }
}

export default MapComponent;
