import React, {PropTypes as T} from 'react';
import Restaurant from './Restaurant.jsx';
import { Col, Panel, FormGroup, FormControl, Button, Glyphicon } from 'react-bootstrap';


export class Listing extends React.Component{

  render(){
    const {places} = this.props;
    return(
      <div className ='restaurantScroll'>
        <h4> {this.props.title} </h4>
        {this.props.places.map(place => {
          return(
            <Restaurant place={place} onClick={this.props.onClick} key={place.id} />
          )
        })}
      </div>
    )
  }
}

export default Listing;
