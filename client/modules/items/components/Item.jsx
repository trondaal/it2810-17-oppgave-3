import React from 'react';
import ReactDOM from 'react-dom';
import { Row, Col, Panel, Glyphicon, Checkbox, FormGroup, Button } from 'react-bootstrap';

import Rating from './Rating.jsx';

class Item extends React.Component {

  render() {
    const {item} = this.props;
    const place = item;
    const style = {'height': '300px'};

    return (
      <Col xs={4}>
        <Panel style={style}>
          <Row>
            <Col xs={10}>
              <h3> {item.name} </h3>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <p> Address: {item.vicinity} </p>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <Rating className='rating' percentage={(place.rating/5)}></Rating>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <p>Price level: {item.priceLevel}/4</p>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>

              <p> Date added: {item.createdAt.getDate()}.{item.createdAt.getMonth()+1}.{item.createdAt.getFullYear()} - {item.createdAt.getHours()}:{item.createdAt.getMinutes()}</p>

              <Button onClick={this.props.onClick} bsStyle="primary" type="submit" value={item._id}> Click two times to remove from favourites </Button>

            </Col>
          </Row>
        </Panel>
      </Col>

    )
  }
};
export default Item;
