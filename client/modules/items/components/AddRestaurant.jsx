import React, {PropTypes as T} from 'react';
import {Row, Col, Button} from 'react-bootstrap';

export class addRestaurant extends React.Component{

render(){
  const {place} = this.props;

  return (
    <div className='addButton'>
        <Button className = 'button' bsStyle="primary" bsSize="small" onClick={this.props.onClick} value={place.id}>Add to favorites</Button>
    </div>
  )
}
}
export default addRestaurant;
