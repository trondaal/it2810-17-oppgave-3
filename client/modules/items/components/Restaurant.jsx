import React, {PropTypes as T} from 'react';
import ReactDOM from 'react-dom';
import Rating from './Rating.jsx';
//import addRestaurant from './addRestaurant.jsx';
import {Row, Col, Button} from 'react-bootstrap';

// import Rating from './Rating.jsx';

export class Restaurant extends React.Component{
  render(){
    const {place} = this.props;
    return(
      //<addRestaurant
      <div className='restaurant-item'>
        <p>Name: {place.name}</p>
        <Rating className='rating' percentage={(place.rating/5)}></Rating>
        <p> {place.vicinity}</p>
        <Button id = "button" bsStyle="primary" ref={"buttonId"} bsSize="small" onClick={this.props.onClick} value={place.id}>Add to favorites</Button>
      </div>
    )
  }
}

export default Restaurant;
