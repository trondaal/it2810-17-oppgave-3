import React, {PropTypes as T} from 'react';
import ReactDOM from 'react-dom';
import {Button, Nav, Navbar, NavDropdown, MenuItem, NavItem, FormGroup, FormControl} from 'react-bootstrap';

// import {handleAutoComplete} from './utils/googleApiHelpers.js';

class SearchBar extends React.Component {
  render(){
    const {google, containerMapState, loc, onAutoComplete} = this.props;
    return(
      <form className ='searchBar'>
      <FormGroup>
        <FormControl type="text" ref="autoCompleteText" onChange={this.handleAutoComplete.bind(this)} placeholder="Search for a place... (ENTER key doesn't work)"/>
      </FormGroup>
    </form>
    )
  }

  handleAutoComplete(){
    if(!this.props.google || !this.props.containerMapState) return;
    const {autoCompleteText} = this.refs;
    const node = ReactDOM.findDOMNode(autoCompleteText);
    var autocomplete = new google.maps.places.Autocomplete(node);

    autocomplete.addListener('place_changed', () => {
      const place = autocomplete.getPlace();
      if(!place.geometry){
        return;
      }(place.geometry.location.lat(), place.geometry.location.lng());
      this.props.onAutoComplete(place.geometry.location);

    });
  }
}


export default SearchBar;
