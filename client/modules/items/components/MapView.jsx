import React, {PropTypes as T} from 'react';
import {Row, Col, Panel} from 'react-bootstrap';
import Map, {GoogleApiWrapper, Marker, InfoWindow} from 'google-maps-react';
import {searchNearby} from './utils/googleApiHelpers';
import Listing from './Listing.jsx';
import SearchBar from './SearchBar.jsx';
import MapComponent from './MapComponent.jsx';

export class MapView extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			places: [],
			pagination: null,
			containerMapState: null,

			//InfoWindow states
			selectedPlace: null,
			selectedVicinity: null,
		}
	}

	onReady(mapProps, map){
		//When the map is ready and mounted, run this
		const {google, center} = this.props;
		this.setState({containerMapState: map});
		this.setState({google: google});
		const opts = {
			location: {lat:63.4214876, lng:10.396553},
			center: {lat:63.4214876, lng:10.396553},
			radius: '1000',
			types: ['restaurant'],
			zoom: 20,
		}
		const mapCenter = opts.center;
		searchNearby(google, map, opts).then((results, pagination) => {
			//We got some results and a pagination object
			this.setState({
				places: results,
				pagination,
				center: mapCenter
			})
		}).catch((status, result) => {
			//There was an error
		})
	}

	onUpdate(loc){
		const{google, center} = this.props;
		const opts = {
			location: loc,
			center: loc,
			radius: '1000',
			types: ['restaurant']
		}
		const mapCenter = opts.center;
		this.setState({
			places: [],
			pagination: null
		})
		searchNearby(google, this.state.containerMapState, opts).then((results, pagination) => {
			//We got some results and a pagination object
			this.setState({
				places: results,
				pagination,
				center: mapCenter
			})
		}).catch((status, result) => {
			//There was an error
		})
	}

	onMarkerClick(item){
		const {place} = item; //place prop
		this.setState({
			selectedPlace: place.name,
			selectedVicinity: place.vicinity,
		})
	}

	onInfoWindowClose(){

	}

	handleAutoComplete(location){
		this.onUpdate(location);
	}

	onClick(event){

		//Getting values from google maps api, then add them to the mongodb.
		var id = event.target.value;
		var name = "";
		var type = "";
		var rating = 0;
		var priceLevel = 0;
		var vicinity = "";
		for (var i = 0; i < this.state.places.length; i++) {
					 if(this.state.places[i].id == id){
						 name = this.state.places[i].name;
						 rating = this.state.places[i].rating;
						 priceLevel = this.state.places[i].price_level;
						 type = this.state.places[i].types[0];
						 vicinity = this.state.places[i].vicinity;
						 this.state.places[i];
					 }
				 }

				 rating = Number(rating);
				 priceLevel = Number(priceLevel);

				 //Adding restaurant to db.
				 Meteor.call('items.create',name, rating, priceLevel, type, vicinity);
	}

	render(){
		if (!this.props.loaded) {
      return <div className='loading'> Loading...</div>
    }
		return(
			<div>
				<Row className='show-grid'>
						<Listing title={'Restaurants'} places={this.state.places} onClick={this.onClick.bind(this)}/>
						<Col xs={4}>
							<Map visible={false} className ='map'  google={this.props.google} onReady={this.onReady.bind(this)} containerStyle={{ width: '190%', height: '110%'}}>
								<SearchBar google={this.props.google} loc={this.state.loc} containerMapState={this.state.containerMapState} onAutoComplete={this.handleAutoComplete.bind(this)}/>
								<MapComponent center={this.state.center} places={this.state.places} selectedPlace={this.state.selectedPlace} selectedVicinity={this.state.selectedVicinity} onMarkerClick={this.onMarkerClick.bind(this)}/>
							</Map>

						</Col>
				</Row>
			</div>
		)
	}
}

export default GoogleApiWrapper({
	//Best practice: apiKey should load from a global variable, but i didn't manage to make it work.
	apiKey: 'AIzaSyCTDaKwjj1RxHX-hi8UjtqJbyHCerKPSRg'
})(MapView);
