/*
Displayes the places that have been added from the mainpage
*/
import React from 'react';
import ReactDOM from 'react-dom';
import Item from './Item.jsx';
import { Row, Col, Glyphicon, Button, FormGroup, Checkbox, DropdownButton, MenuItem, SplitButton} from 'react-bootstrap';
import ItemProgress from './ItemProgress.jsx';

export class ItemList extends React.Component{

//Initializes the state of the component
  constructor(props){
		super(props);
		this.state = {
			sortedCol: this.props.dateAdded,
      activeTab: "dateAdded",
      checkedBar: false,
      checkedRating: false,
      initLoad: true,
      items: [],
      loaded: 0,
      rest: 9,
      currentState: 1
		}

    this.sort = this.sort.bind(this);
    this.loadItems = this.loadItems.bind(this);
    this.loadItemsHandler = this.loadItemsHandler.bind(this);
	}


  componentDidMount(){
    this.loadItems();
  }

//Logic for filtering. Checks which of the checkbox who is selected and prevents one from selecting both at the same time.
  onChange(evt){
  if(evt.target.value=="bar"&& evt.target.checked==true){
    this.setState({
      sortedCol: this.props.filterBar,
      checkedBar: true,
      checkedRating: false,
      test: false,
      currentState: 3
    })
  }else if(evt.target.value=="bar"&& evt.target.checked==false){
    this.setState({
      sortedCol: this.props.dateAdded,
      checkedBar: false,
      checkedRating: false,
      currentState: 1
    })
  }else if(evt.target.value=="rating"&& evt.target.checked==true){
      this.setState({
        sortedCol: this.props.filterRating,
        checkedBar: false,
        checkedRating: true,
        currentState: 4
      })
    }else if(evt.target.value=="rating"&& evt.target.checked==false){
      this.setState({
        sortedCol: this.props.dateAdded,
        checkedBar: false,
        checkedRating: false,
        currentState: 1
      })
    }
}
//Check which menuitem who is picked, rating or on date added.
  sort(evt){

    if(evt=="rating"){
      this.setState({
        sortedCol: this.props.rating,
        activeTab: "rating",
        currentState: 2
      })
    }else{
      this.setState({
        sortedCol: this.props.dateAdded,
        activeTab: "dateAdded",
        currentState: 1
      })
    }
  }
//Removes item from collection.
  onClick(evt){
    Meteor.call('items.remove',evt.target.value);
    this.reRender();
  }


  //Running nextItems function from the back-end
  loadItems(){
    const itemsCollection = this.props.nextItems(this.state.loaded);
    const items = itemsCollection.map(item => <Item className='item' key={item._id} item={item} />);

    if(itemsCollection.count() > 0){
      this.setState(Object.assign({}, this.state, {
        items: this.state.items.concat(items),
        loaded: this.state.loaded + itemsCollection.count(),
        rest: (this.state.loaded + 9) > this.props.totalItems ? this.props.totalItems - this.state.loaded : this.state.rest
      }));
    }
  }

  loadItemsHandler(){
    this.loadItems();
  }

  reRender(){
        if(this.state.currentState==1){
        this.setState({
          sortedCol: this.props.dateAdded
        })}else if(this.state.currentState==2){
          this.setState({
            sortedCol: this.props.rating
          })
        }else if(this.state.currentState==3){
          this.setState({
            sortedCol: this.props.filterBar
          })}else if(this.state.currentState==4){
            this.setState({
              sortedCol: this.props.filterRating
            })
        };
  }

  render(){
    const {dateAdded, rating, filterBar, filterRating, totalItems, nextItems, removeItem} = this.props;
    return(

      <Row>

        <Row className="show-grid2">

          <Col xs={10} className = 'show-grid3'>

{/*Dropdown menu.*/}
            <SplitButton title="Sort on" bsStyle="primary" key={2} id={2} onSelect={this.sort.bind(this)}>
                <MenuItem className={(this.state.activeTab === "dateAdded") ? "active" : ""} eventKey="dateAdded">Date added</MenuItem>
                <MenuItem className={(this.state.activeTab === "rating") ? "active" : ""} eventKey="rating">Rating</MenuItem>
            </SplitButton>

            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
{/*Checkboxes*/}
            <label className="btn btn-primary">
              <input type="checkbox" autoComplete="off" className={(this.state.checkedBar === "bar") ? "active" : ""} defaultValue="bar" checked={this.state.checkedBar} onChange={this.onChange.bind(this)}/> Filter by bars
            </label>
            <label className="btn btn-primary">
              <input type="checkbox" autoComplete="off" className={(this.state.checkedRating === "rating") ? "active" : ""} defaultValue="rating" checked={this.state.checkedRating} onChange={this.onChange.bind(this)} /> Filter by places with rating greater or equal to 4
            </label>

          </Col>
          <br/>
          <br/>
          {/*Function for rendering all the items in sortedCol. Depends on what filter or sorting who is choosed.*/}
          {this.state.sortedCol.map(item => (<Item className = 'item' key={item._id} item={item} onClick={this.onClick.bind(this)} />))}
          {/*{this.state.items}*/}
        </Row>
        {/* Sorting and dynamic loading of the page doesn't quite work together. To get dynamic loading to work. Just comment out line 167 and uncomment line 168 and 172-177... This was really unfortunate */}

        {/*<Row>
          <div className="load-more-row">
            <Button onClick={ () => this.loadItemsHandler() }>Load more</Button>
            <p> Loaded {this.state.loaded}/{this.props.totalItems} favorites</p>
          </div>
        </Row>*/}

      </Row>

    )

  }

}
export default ItemList;
