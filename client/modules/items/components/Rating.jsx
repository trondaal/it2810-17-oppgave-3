import React, {PropTypes as T} from 'react';

const RatingIcon = (props) => (<span>★</span>);

export class Rating extends React.Component {
  render(){
    const {percentage, places} = this.props;
    const style = {
      width: `${(percentage || 0) * 100}%`
    }

    return (
      <div className='sprite'>
        <div className='top-star' style={style}>
          <RatingIcon />
          <RatingIcon />
          <RatingIcon />
          <RatingIcon />
          <RatingIcon />
        </div>
        <div className='bottom-star'>
          <RatingIcon />
          <RatingIcon />
          <RatingIcon />
          <RatingIcon />
          <RatingIcon />
        </div>
      </div>
    )
  }
}

export default Rating;
