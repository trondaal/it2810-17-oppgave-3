export default {
  create({Meteor, LocalState, FlowRouter}, name, rating, priceLevel, type, due) {
    if (!name) {
      return LocalState.set('CREATE_ITEM_ERROR', 'Item name is required.');
    }

    LocalState.set('CREATE_ITEM_ERROR', null);

    Meteor.call('items.create', name, rating, priceLevel, type, vicinity, (err) => {
      if (err) {
        return LocalState.set('SAVING_ERROR', err.message);
      }
    });
    FlowRouter.go('/myfavorites');
  },

  clearErrors({LocalState}) {
    return LocalState.set('SAVING_ERROR', null);
  }
};
